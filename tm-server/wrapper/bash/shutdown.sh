#!/usr/bin/env bash

PORT="8080"
if [ -n "$1" ]; then
  PORT=$1;
fi
FILE_PID="server-$PORT.pid"

echo  "SHUTDOWN SERVER AT $PORT...";

if [ ! -f ./$FILE_PID ]; then
	echo "$FILE_PID pid not found"
	exit 1;
fi

echo "KILL PROCESS WITH PID "$(cat ./$FILE_PID);
kill -9 $(cat ./$FILE_PID)
rm ./$FILE_PID
echo "OK";
