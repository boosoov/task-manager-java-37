package com.rencredit.jschool.boruak.taskmanager.exception.unknown;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class UnknownUserException extends AbstractException {

    public UnknownUserException() {
        super("Error! Unknown user...");
    }

    public UnknownUserException(final String user) {
        super("Error! Unknown user ``" + user + "``...");
    }

}
