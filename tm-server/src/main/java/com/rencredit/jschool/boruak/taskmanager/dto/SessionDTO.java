package com.rencredit.jschool.boruak.taskmanager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "app_session")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionDTO extends AbstractDTO implements Cloneable {
    public static final long serialVersionUID = 1L;

    @Nullable
    @Column(name = "timestamp")
    private Long timestamp;

    @Nullable
    @Column(name = "user_Id")
    private String userId;

    @Nullable
    @Column(name = "signature")
    private String signature;

    public SessionDTO() {
    }

    public SessionDTO(
            @Nullable final Long timestamp,
            @Nullable final String userId,
            @Nullable final String signature
    ) {
        this.timestamp = timestamp;
        this.userId = userId;
        this.signature = signature;
    }

    @Nullable
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@Nullable final Long timestamp) {
        this.timestamp = timestamp;
    }

    @Nullable
    public String getUserId() {
        return userId;
    }

    public void setUserId(@Nullable final String userId) {
        this.userId = userId;
    }

    @Nullable
    public String getSignature() {
        return signature;
    }

    public void setSignature(@Nullable final String signature) {
        this.signature = signature;
    }

    @Nullable
    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @NotNull
    @Override
    public String toString() {
        return "Session{" +
                "id='" + super.getId() + '\'' +
                ", timestamp=" + timestamp +
                ", userId='" + userId + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }


}
