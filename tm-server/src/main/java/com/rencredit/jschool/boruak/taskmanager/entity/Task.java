package com.rencredit.jschool.boruak.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "app_task")
public class Task extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String description = "";

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @ManyToOne
    private Project project;

    public Task() {
    }

    public Task(
            @NotNull final User user,
            @NotNull final Project project,
            @NotNull final String name

    ) {
        this.user = user;
        this.project = project;
        this.name = name;
    }

    public Task(
            @NotNull final User user,
            @NotNull final Project project,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.user = user;
        this.project = project;
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + super.getId() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + user + '\'' +
                '}';
    }

}
