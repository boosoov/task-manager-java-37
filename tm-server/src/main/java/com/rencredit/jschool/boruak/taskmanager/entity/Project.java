package com.rencredit.jschool.boruak.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "app_project")
public class Project extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String description = "";

    @Nullable
    @ManyToOne
    private User user;

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE)
    private List<Task> tasks = new ArrayList<>();

    public Project() {
    }

    public Project(
            @NotNull final User user,
            @NotNull final String name
    ) {
        this.user = user;
        this.name = name;
    }

    public Project(
            @NotNull final User user,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.user = user;
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return "Project{" +
                "id='" + super.getId() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + user + '\'' +
                '}';
    }

}
