package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminUserEndpoint {

    @Nullable
    @WebMethod
    UserDTO lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws DeniedAccessException, EmptyLoginException, EmptyUserException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @Nullable
    @WebMethod
    UserDTO unlockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws DeniedAccessException, EmptyLoginException, EmptyUserException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

}
