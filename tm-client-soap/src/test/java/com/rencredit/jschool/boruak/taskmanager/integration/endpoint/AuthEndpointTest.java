package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.junit.experimental.categories.Category;

@Category(IntegrationWithServerTestCategory.class)
public class AuthEndpointTest {
//
//    @NotNull IEndpointLocator endpointLocator;
//    @NotNull AuthEndpoint authEndpoint;
//    @NotNull UserEndpoint userEndpoint;
//    @NotNull SessionEndpoint sessionEndpoint;
//    @NotNull SessionDTO session;
//
//    @Before
//    public void init() throws EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyHashLineException_Exception, DeniedAccessException_Exception, BusyLoginException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
//        endpointLocator = new EndpointLocator();
//        authEndpoint = endpointLocator.getAuthEndpoint();
//        userEndpoint = endpointLocator.getUserEndpoint();
//        sessionEndpoint = endpointLocator.getSessionEndpoint();
//
//        session = sessionEndpoint.openSession("admin", "admin");
//        userEndpoint.clearAllUser(session);
//        session = sessionEndpoint.openSession("admin", "admin");
//    }
//
//    @After
//    public void clearAll() throws DeniedAccessException_Exception {
////        sessionEndpoint.closeSession(session);
//    }
//
//    @Test
//    public void testLogIn() throws EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyHashLineException_Exception, DeniedAccessException_Exception, EmptyRoleException_Exception, BusyLoginException_Exception {
//        @NotNull final SessionDTO sessionTest = authEndpoint.logIn("test", "test");
//        Assert.assertNotNull(sessionTest);
//    }
//
//    @Test
//    public void testLogOut() throws DeniedAccessException_Exception {
//        @NotNull final Result result = authEndpoint.logOut(session);
//        Assert.assertTrue(result.isSuccess());
//    }
//
//    @Test
//    public void testGetUserId() throws EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
//        @NotNull final String userId = authEndpoint.getUserId(session);
//        @NotNull final UserDTO user = userEndpoint.getUserByLogin(session,"admin");
//        Assert.assertEquals(user.getId(), userId);
//    }
//
//    @Test
//    public void testCheckRoles() throws NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
//        @NotNull final List<Role> rolesAdmin = Arrays.asList(Role.ADMIN);
//        authEndpoint.checkRoles(session, rolesAdmin);
//    }
//
//    @Test(expected = DeniedAccessException_Exception.class)
//    public void testCheckRolesNotCorrectRole() throws NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
//        @NotNull final List<Role> rolesUser = Arrays.asList(Role.USER);
//        authEndpoint.checkRoles(session, rolesUser);
//    }
//
//    @Test
//    public void testRegistrationLoginPassword() throws EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, BusyLoginException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
//        authEndpoint.registrationLoginPassword("login01", "password01");
//        @NotNull UserDTO user = userEndpoint.getUserByLogin(session,"login01");
//        Assert.assertNotNull(user);
//        userEndpoint.removeUserByLogin(session,"login01");
//        user = userEndpoint.getUserByLogin(session,"login01");
//        Assert.assertNull(user);
//    }
//
//    @Test
//    public void testRegistrationLoginPasswordFirstName() throws EmptyFirstNameException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyEmailException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
//        authEndpoint.registrationLoginPasswordFirstName("login02", "password02", "firstName");
//        @NotNull UserDTO user = userEndpoint.getUserByLogin(session,"login02");
//        Assert.assertNotNull(user);
//        Assert.assertEquals("firstName", user.getFirstName());
//        userEndpoint.removeUserByLogin(session,"login02");
//        user = userEndpoint.getUserByLogin(session,"login02");
//        Assert.assertNull(user);
//    }
//
//    @Test
//    public void testRegistrationLoginPasswordRole() throws EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
//        authEndpoint.registrationLoginPasswordRole(session, "login03", "password03", Role.USER);
//        @NotNull UserDTO user = userEndpoint.getUserByLogin(session,"login03");
//        Assert.assertNotNull(user);
//        Assert.assertEquals(Role.USER, user.getRole());
//        userEndpoint.removeUserByLogin(session,"login03");
//        user = userEndpoint.getUserByLogin(session,"login03");
//        Assert.assertNull(user);
//    }

}
