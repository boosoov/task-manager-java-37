package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyTaskException extends AbstractClientException {

    public EmptyTaskException() {
        super("Error! Task not exist...");
    }

}
