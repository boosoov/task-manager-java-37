package com.rencredit.jschool.boruak.taskmanager.listener.project;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProjectListShowListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show projects list.";
    }

    @Override
    @EventListener(condition = "@projectListShowListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws EmptyUserIdException_Exception, DeniedAccessException_Exception, EmptyUserException {
        System.out.println("[LIST PROJECTS]");

        @NotNull final SessionDTO session = systemObjectService.getSession();
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectByUserId(session);
        @NotNull int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(index + ". ");
            ViewUtil.showProject(project);
            index++;
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
