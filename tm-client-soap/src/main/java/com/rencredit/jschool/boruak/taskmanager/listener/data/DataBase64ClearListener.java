package com.rencredit.jschool.boruak.taskmanager.listener.data;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DataBase64ClearListener extends AbstractListener {

    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove base64 data.";
    }

    @Override
    @EventListener(condition = "@dataBase64ClearListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws IOException_Exception, NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        @NotNull final SessionDTO session = systemObjectService.getSession();
        adminEndpoint.clearBase64(session);
        System.out.println("[OK]");

    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
