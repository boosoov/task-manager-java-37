<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../include/_header.jsp"/>

<h2>TASK EDIT</h2>

<form:form action="/task/edit/${task.id}" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id" />

    <p>
        <div style="margin-bottom: 5px;">NAME:</div>
        <div><form:input type="text" path="name" /></div>
    </p>

    <p>
        <div style="margin-bottom: 5px;">DESCRIPTION:</div>
        <div><form:input type="text" path="description" /></div>
    </p>

    <p>
        <div style="margin-bottom: 5px;">STATUS:</div>
        <div>
            <form:select path="status" >
                <form:option value="${null}" label="--- // ---" />
                <form:options items="${statuses}" itemLabel="displayName" />
            </form:select>
        </div>
    </p>

    <p>
        <div style="margin-bottom: 5px;">DATE BEGIN:</div>
        <div><form:input type="date" path="dateStart" /></div>
    </p>

    <p>
        <div style="margin-bottom: 5px;">DATE FINISH:</div>
        <div><form:input type="date" path="dateFinish" /></div>
    </p>

    <p>
    <div style="margin-bottom: 5px;">PROJECT:</div>
        <div>
            <form:select path="projectId" >
                <form:option value="${null}" label="--- // ---" />
                <form:options items="${projects}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>
    </p>

    <button type="submit">SAVE TASK</button>

</form:form>



<jsp:include page="../include/_footer.jsp"/>

