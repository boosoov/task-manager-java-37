package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.ITaskRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    @PostMapping
    public void create(@RequestBody TaskDTO task) throws EmptyTaskException, EmptyUserIdException {
        taskService.create(task);
    }

    @Override
    @Nullable
    @GetMapping("/${id}")
    public TaskDTO findOneByIdDTO(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        return taskService.findOneByIdDTO(user.getId(), id);
    }

    @Override
    @GetMapping("/exist/${id}")
    public boolean existsById(@PathVariable("id") String id) {
        return taskService.existsById(id);
    }

    @Override
    @Transactional
    @DeleteMapping("/${id}")
    public void deleteOneById(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        taskService.deleteOneById(user.getId(), id);
    }

}
