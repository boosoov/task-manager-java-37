package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISessionService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.repository.dto.ISessionRepositoryDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.ISessionRepositoryEntity;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import com.rencredit.jschool.boruak.taskmanager.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Autowired
    private ISessionRepositoryDTO repositoryDTO;

    @Autowired
    private ISessionRepositoryEntity repositoryEntity;

    @Nullable
    @Override
    @Transactional
    public SessionDTO open(@Nullable final String login, @Nullable final String password) throws EmptyPasswordException, EmptyLoginException, EmptyHashLineException, DeniedAccessException, EmptyUserException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        final boolean check = checkUserAccess(login, password);
        if (!check) throw new DeniedAccessException();
        final UserDTO user = userService.getByLoginDTO(login);
        if (user == null) throw new EmptyUserException();
        if (user.isLocked()) throw new DeniedAccessException();
        final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @Nullable final SessionDTO sessionSign = sign(session);
        repositoryDTO.save(session);
        return sessionSign;
    }

    @Override
    @Transactional
    public void close(@Nullable final SessionDTO session) throws DeniedAccessException {
        validate(session);
        repositoryEntity.deleteById(session.getId());
    }

    @Override
    @Transactional
    public void closeAll(@Nullable final SessionDTO session) throws DeniedAccessException {
        validate(session);
        repositoryEntity.deleteAll();
    }

    @Override
    @Transactional
    public void closeAll() {
        repositoryEntity.deleteAll();
    }

    @Nullable
    @Override
    public UserDTO getUser(@Nullable final SessionDTO session) throws DeniedAccessException, EmptyIdException {
        @Nullable final String userId = getUserId(session);
        return userService.getByIdDTO(userId);
    }

    @Nullable
    @Override
    public String getUserId(@Nullable final SessionDTO session) throws DeniedAccessException {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<SessionDTO> getListSession(@Nullable final SessionDTO session) throws DeniedAccessException {
        validate(session);
        return repositoryDTO.findAll();
    }

    @Nullable
    @Override
    public SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final SessionDTO session) throws DeniedAccessException {
        if (session == null) throw new DeniedAccessException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new DeniedAccessException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new DeniedAccessException();
        if (session.getTimestamp() == null) throw new DeniedAccessException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new DeniedAccessException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new DeniedAccessException();

        if (!repositoryDTO.existsById(session.getId())) throw new DeniedAccessException();
    }


    @Override
    public void validate(@Nullable final SessionDTO session, @Nullable final Role role) throws DeniedAccessException, EmptyIdException {
        if (role == null) throw new DeniedAccessException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = userService.getByIdDTO(userId);
        if (user == null) throw new DeniedAccessException();
        if (!role.equals((user.getRole()))) throw new DeniedAccessException();
    }

    @Override
    public boolean checkUserAccess(@NotNull final String login, @NotNull final String password) throws EmptyHashLineException, EmptyLoginException {
        @Nullable final UserDTO user = userService.getByLoginDTO(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @Transactional
    public void clearAll() {
        repositoryDTO.deleteAll();;
    }

}
