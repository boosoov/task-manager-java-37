package com.rencredit.jschool.boruak.taskmanager.controller;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Status;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @ModelAttribute("statuses")
    public Status[] getStatus() {
        return Status.values();
    }

    @ModelAttribute("projects")
    private List<ProjectDTO> getProjects() {
        return projectService.getList();
    }

    @GetMapping("/task/create")
    public String create() throws EmptyLoginException, EmptyNameException, EmptyUserIdException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        taskService.create(user.getId(), "Empty");
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) throws EmptyLoginException, EmptyIdException, EmptyUserIdException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        taskService.deleteOneById(user.getId(), id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) throws EmptyLoginException, EmptyIdException, EmptyUserIdException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        final TaskDTO task = taskService.findOneByIdDTO(user.getId(), id);
        return new ModelAndView("task-edit", "task", task);
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") TaskDTO task,
                BindingResult result
    ) throws EmptyUserIdException, EmptyTaskException {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.create(task);
        return "redirect:/tasks";
    }

}
