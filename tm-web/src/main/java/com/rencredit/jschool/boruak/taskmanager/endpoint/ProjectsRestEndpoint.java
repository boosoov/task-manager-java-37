package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IProjectsRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectsRestEndpoint implements IProjectsRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @GetMapping
    public List<ProjectDTO> getListDTO() {
        return projectService.getList();
    }

    @Override
    @PostMapping
    public List<ProjectDTO> saveAll(@RequestBody List<ProjectDTO> list) {
        return projectService.saveAll(list);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

    @Override
    @DeleteMapping("/all")
    @Transactional
    public void deleteAll() {
        projectService.deleteAll();
    }

    @Override
    @DeleteMapping
    public void deleteAll(@RequestBody List<ProjectDTO> list) {
        projectService.deleteAll(list);
    }

}
