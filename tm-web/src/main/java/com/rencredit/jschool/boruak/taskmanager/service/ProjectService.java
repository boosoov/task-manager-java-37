package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IProjectService;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.repository.dto.IProjectRepositoryDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IProjectRepositoryEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectService extends AbstractService<ProjectDTO> implements IProjectService {

    @Autowired
    private IProjectRepositoryDTO repositoryDTO;

    @Autowired
    private IProjectRepositoryEntity repositoryEntity;

    @Override
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final ProjectDTO project = new ProjectDTO(userId, name);
        repositoryDTO.save(project);
    }

    @Override
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @NotNull final ProjectDTO project = new ProjectDTO(userId, name, description);
        repositoryDTO.save(project);
    }

    @Override
    @Transactional
    public void create(@Nullable final String userId, @Nullable final ProjectDTO project) throws EmptyProjectException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyProjectException();

        project.setUserId(userId);
        repositoryDTO.save(project);
    }

    @Override
    @Transactional
    public void create(@Nullable final String userId, @Nullable final Project project) throws EmptyProjectException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyProjectException();

        repositoryEntity.save(project);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserIdDTO(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final List<ProjectDTO> projects = repositoryDTO.findAllByUserId(userId);
        return projects;
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        repositoryEntity.deleteAllByUserId(userId);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndexDTO(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        @NotNull final ProjectDTO project = findAllByUserIdDTO(userId).get(index);
        return project;
    }

    @Nullable
    @Override
    public ProjectDTO findOneByNameDTO(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final ProjectDTO project = repositoryDTO.findByUserIdAndName(userId, name);
        return project;
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIdDTO(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @Nullable final Optional<ProjectDTO> opt = repositoryDTO.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }

    @Nullable
    @Override
    public Project findOneEntityById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @Nullable final Optional<Project> opt = repositoryEntity.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws EmptyProjectException, EmptyNameException, EmptyIdException, EmptyUserIdException, EmptyDescriptionException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @Nullable final ProjectDTO project = findOneByIdDTO(userId, id);
        if (project == null) throw new EmptyProjectException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        repositoryDTO.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws EmptyProjectException, EmptyNameException, IncorrectIndexException, EmptyUserIdException, EmptyDescriptionException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @Nullable final ProjectDTO project = findOneByIndexDTO(userId, index);
        if (project == null) throw new EmptyProjectException();
        project.setName(name);
        project.setDescription(description);
        repositoryDTO.save(project);
        return project;
    }

    @Override
    @Transactional
    public void delete(@Nullable final String userId, @Nullable final ProjectDTO project) throws EmptyProjectException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyProjectException();

        repositoryEntity.deleteById(project.getId());
    }

    @Override
    @Transactional
    public void deleteOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException, EmptyProjectException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        @Nullable final ProjectDTO project = findOneByIndexDTO(userId, index);
        if (project == null) throw new EmptyProjectException();
        @NotNull final String projectId = project.getId();
        repositoryEntity.deleteById(projectId);
    }

    @Override
    @Transactional
    public void deleteOneByName(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        repositoryEntity.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void deleteOneById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        repositoryEntity.deleteById(id);
    }

    @NotNull
    @Override
    public List<ProjectDTO> getList() {
        @NotNull final List<ProjectDTO> projects = repositoryDTO.findAll();
        return projects;
    }

    @Override
    @Transactional
    public void deleteAll() {
        repositoryEntity.deleteAll();
    }

    public <S extends ProjectDTO> List<S> saveAll(Iterable<S> iterable) {
        return repositoryDTO.saveAll(iterable);
    }

    public long count() {
        return repositoryDTO.count();
    }

    public void deleteAll(Iterable<? extends ProjectDTO> iterable) {
        repositoryDTO.deleteAll(iterable);
    }

    public boolean existsById(String s) {
        return repositoryDTO.existsById(s);
    }

}
