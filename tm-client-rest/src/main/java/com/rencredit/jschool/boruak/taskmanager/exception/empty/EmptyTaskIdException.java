package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyTaskIdException extends AbstractException {

    public EmptyTaskIdException() {
        super("Error! Task Id is empty...");
    }

}
