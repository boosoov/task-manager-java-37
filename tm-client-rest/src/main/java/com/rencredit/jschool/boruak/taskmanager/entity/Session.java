package com.rencredit.jschool.boruak.taskmanager.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Session extends AbstractEntity implements Cloneable {

    public static final long serialVersionUID = 1L;

    @Nullable
    private Long timestamp;

    @Nullable
    private User user;

    @Nullable
    private String signature;

    public Session() {
    }

    public Session(
            @Nullable final Long timestamp,
            @Nullable final User user,
            @Nullable final String signature
    ) {
        this.timestamp = timestamp;
        this.user = user;
        this.signature = signature;
    }

    @Nullable
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@Nullable final Long timestamp) {
        this.timestamp = timestamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Nullable
    public String getSignature() {
        return signature;
    }

    public void setSignature(@Nullable final String signature) {
        this.signature = signature;
    }

    @Nullable
    @Override
    public com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO clone() {
        try {
            return (com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @NotNull
    @Override
    public String toString() {
        return "Session{" +
                "id='" + super.getId() + '\'' +
                ", timestamp=" + timestamp +
                ", userId='" + user + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }

}
