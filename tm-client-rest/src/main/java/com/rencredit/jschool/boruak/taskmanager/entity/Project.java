package com.rencredit.jschool.boruak.taskmanager.entity;

import com.rencredit.jschool.boruak.taskmanager.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
public class Project extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String description = "";

    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    private User user;

    @NotNull
    private List<Task> tasks = new ArrayList<>();

    public Project() {
    }

    public Project(
            @NotNull final User user,
            @NotNull final String name
    ) {
        this.user = user;
        this.name = name;
    }

    public Project(
            @NotNull final User user,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.user = user;
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return "Project{" +
                "id='" + super.getId() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + user + '\'' +
                '}';
    }

}
