package restendpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.ITaskRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ITaskRestEndpointTest {

    ITaskRestEndpoint taskRestEndpoint;

    @Before
    public void init()  {
        taskRestEndpoint = ITaskRestEndpoint.client("http://localhost:8080/");
    }

    @After
    public void clearAll() {

    }

    @Test
    public void test() throws EmptyLoginException, EmptyUserIdException, EmptyIdException, EmptyTaskException {
        final TaskDTO task = new TaskDTO();
        task.setDescription("111111");
        taskRestEndpoint.create(task);

        final TaskDTO taskFromWeb = taskRestEndpoint.findOneByIdDTO(task.getId());
        Assert.assertEquals(task.getId(), taskFromWeb.getId());

        Assert.assertTrue(taskRestEndpoint.existsById(task.getId()));

        taskRestEndpoint.deleteOneById(task.getId());
        Assert.assertFalse(taskRestEndpoint.existsById(task.getId()));
    }

}
